package backendservices;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.avis.avistechbd.ngodemo.R;
import com.avis.avistechbd.ngodemo.customview.SignInAct;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SendGetLocToFromFirebase {

    //Set_App_Context
    private Context context;
    //Customer_Local_Database_With_Key<>Value_pair
    private SharedPreferences mySharedPref;
    //FireBase_Instance_initiate
    private FirebaseDatabase database;

    public SendGetLocToFromFirebase(Context context, SharedPreferences mySharedPref, FirebaseDatabase database) {
        this.context = context;
        this.mySharedPref = mySharedPref;
        this.database = database;
    }

    //get_Customer_phn_no (AuthPhone number->Create new Acc / ForgotPass)
    public String getLocalPhone(){
        String uPhone = "+88017000000";
        try {
            uPhone = mySharedPref.getString("custPhn", uPhone);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return uPhone;
    }

    //get_Customer_phn_no (LoggedInPhn)
    public String getLocalLoginPhone(){
        String uPhone = "+88017000000";
        try {
            uPhone = mySharedPref.getString("custPhnLogged", uPhone);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return uPhone;
    }

    //set_Data_Into_Firebase
    public boolean setCustomerLocationInFireDB(String uPhone,String myLatt,String myLong){
        boolean flag = false;
        try{
            DatabaseReference myRef = database.getReference(uPhone);
            //Setting_data
            myRef.child("uLatt").setValue(myLatt);
            myRef.child("uLong").setValue(myLong);
            flag = true;
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(context, R.string.set_customer_loc_fire_error, Toast.LENGTH_SHORT).show();
        }
        return flag;
    }

    //set_Data_Into_Firebase
    public boolean setCustInfoFireDB(String uPhone,String uName,String uGender,String uPass){
        boolean flag = false;
        try{
            DatabaseReference myRef = database.getReference(uPhone);
            Log.d("snap try",myRef.toString());
            //Setting_data
            myRef.child("uName").setValue(uName);
            myRef.child("uGender").setValue(uGender);
            myRef.child("ePass").setValue(uPass);
            //deFault
            myRef.child("eJob").setValue("Customer");
            myRef.child("eMail").setValue("customer@avis.com");
            myRef.child("sCost").setValue("0-tk");
            myRef.child("uWork").setValue("1");
            myRef.child("uLog").setValue("0");
            flag = true;
        }catch (Exception e){
            e.printStackTrace();
            Log.d("snap catch",e.getMessage());
            Toast.makeText(context, "Account Create DB Error, Try again latter >> ", Toast.LENGTH_SHORT).show();
        }
        return flag;
    }

    //set_New_Pass
    public boolean setNewPassDB(String uPhone, String uPass){
        boolean flag = false;
        try{
            DatabaseReference myRef = database.getReference(uPhone);
            //Setting_data
            myRef.child("ePass").setValue(uPass);
            flag = true;
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(context, "DB Error, Try again latter", Toast.LENGTH_SHORT).show();
        }
        return flag;
    }

    //set_Login_flag
    public boolean setLoginDB(String uPhone){
        boolean flag = false;
        try{
            DatabaseReference myRef = database.getReference(uPhone);
            //Setting_data
            myRef.child("uLog").setValue("1");
            flag = true;
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(context, "DB Error, Try again latter", Toast.LENGTH_SHORT).show();
        }
        return flag;
    }

    //set_LogOut_flag
    public boolean setLogOutDB(String uPhone){
        boolean flag = false;
        try{
            DatabaseReference myRef = database.getReference(uPhone);
            //Setting_data
            myRef.child("uLog").setValue("0");
            flag = true;
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(context, "DB Error, Try again latter", Toast.LENGTH_SHORT).show();
        }
        return flag;
    }

}
