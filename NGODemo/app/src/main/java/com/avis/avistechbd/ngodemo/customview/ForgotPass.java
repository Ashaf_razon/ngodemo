package com.avis.avistechbd.ngodemo.customview;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.avis.avistechbd.ngodemo.R;
import com.avis.avistechbd.ngodemo.model.Vaidiation;
import com.avis.avistechbd.ngodemo.presenter.UserValidationInterface;
import com.avis.avistechbd.ngodemo.view.ShowValidationError;
import com.google.firebase.database.FirebaseDatabase;

import backendservices.SendGetLocToFromFirebase;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotPass extends AppCompatActivity implements ShowValidationError{

    @BindView(R.id.forgotSetPass)
    EditText forgotSetPass;
    @BindView(R.id.forgotConfPass)
    EditText forgotConfPass;

    private UserValidationInterface getValidation;
    private String uPass, uConfPass;
    //UsersPhoneSave
    private SharedPreferences mySharedPref;
    private String myRef = "myCustPhone"; //Reference_Key
    private Context context;
    private FirebaseDatabase database;
    private SendGetLocToFromFirebase mGetPhoneNsetFire;
    private ProgressDialog pd;
    private boolean forgFlag;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass);
        ButterKnife.bind(this);
        getValidation = new Vaidiation( ForgotPass.this);
        context = ForgotPass.this;
        database = FirebaseDatabase.getInstance();
        mySharedPref = getSharedPreferences(myRef, Context.MODE_PRIVATE);
        mGetPhoneNsetFire = new SendGetLocToFromFirebase(context,mySharedPref,database);
        pd = new ProgressDialog(ForgotPass.this);
        pd.setTitle("Password update");
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.setIndeterminate(false);
        handler = new Handler(Looper.getMainLooper());
    }

    @OnClick(R.id.forgotChangePass)
    public void onViewClicked() {
        try{
            uPass = forgotSetPass.getText().toString().replaceAll("\\s+","");
            uConfPass = forgotConfPass.getText().toString().replaceAll("\\s+","");
            if(getValidation.singlePassValidation(uPass) && getValidation.singlePassValidation(uConfPass)) {
                getValidation.checkPasswordMatching(uPass, uConfPass);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void wrongPassMsg() {
        forgotSetPass.setError("Password Length must be (4-12)");
        forgotConfPass.setError("Password Length must be (4-12)");
        forgotSetPass.requestFocus();
        forgotConfPass.requestFocus();
    }

    @Override
    public void wrongPhoneMsg() {

    }

    @Override
    public void wrongNameValidationMsg() {

    }

    @Override
    public void passMatchingMsg() {
        Toast.makeText(this, "Please enter same password", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void passwordMatched() {
        pd.show();
        forgFlag = false;
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    forgFlag = mGetPhoneNsetFire.setNewPassDB(mGetPhoneNsetFire.getLocalPhone(),uPass);
                    Thread.sleep(500);
                }catch (Exception e){
                    e.printStackTrace();
                }

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(forgFlag){
                            pd.dismiss();
                            Toast.makeText(context, "Successfully Password updated", Toast.LENGTH_SHORT).show();
                            Intent goLog = new Intent(ForgotPass.this,SignInAct.class);
                            startActivity(goLog);
                        }else {
                            Toast.makeText(context, "Failed, please try again latter", Toast.LENGTH_SHORT).show();
                            pd.dismiss();
                        }
                    }
                });
            }
        }).start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }
}
