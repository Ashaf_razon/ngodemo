package com.avis.avistechbd.ngodemo.customview;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.avis.avistechbd.ngodemo.R;
import com.avis.avistechbd.ngodemo.model.Vaidiation;
import com.avis.avistechbd.ngodemo.presenter.UserValidationInterface;
import com.avis.avistechbd.ngodemo.view.ShowValidationError;
import com.google.firebase.database.FirebaseDatabase;

import backendservices.SendGetLocToFromFirebase;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreateUserAcc extends AppCompatActivity implements ShowValidationError{

    @BindView(R.id.getUname)
    EditText getUname;
    @BindView(R.id.getUserGenderMale)
    RadioButton getUserGenderMale;
    @BindView(R.id.getUserGenderFemale)
    RadioButton getUserGenderFemale;
    @BindView(R.id.getUpass)
    EditText getUpass;
    @BindView(R.id.getUconfPass)
    EditText getUconfPass;
    @BindView(R.id.radioSex)
    RadioGroup radioSexGroup;
    RadioButton radioButton;

    private UserValidationInterface getValidation;
    private String uName, uGender, uPass, uConfPass;
    //UsersPhoneSave
    private SharedPreferences mySharedPref;
    private String myRef = "myCustPhone"; //Reference_Key
    private Context context;
    private FirebaseDatabase database;
    private SendGetLocToFromFirebase mGetPhoneNsetFire;
    private Handler handler;
    private ProgressDialog pd;
    private boolean getRes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user_acc);
        ButterKnife.bind(this);
        getValidation = new Vaidiation( CreateUserAcc.this);
        initializePrimary();
    }

    @OnClick(R.id.createUacc)
    public void onViewClicked() {
        try{
            uName = getUname.getText().toString();
            uGender = getRadioText();
            uPass = getUpass.getText().toString().replaceAll("\\s+","");
            uConfPass = getUconfPass.getText().toString().replaceAll("\\s+","");
            if(getValidation.singlePassValidation(uPass) && getValidation.singlePassValidation(uConfPass) && getValidation.singleNameValidation(uName)){
                validationSuccess();
            }
        }catch (Exception e){
            e.printStackTrace();
            fieldError();
        }
    }

    private void fieldError() {
        Toast.makeText(this, "Field must not be empty", Toast.LENGTH_SHORT).show();
    }

    private String getRadioText() {
        try{
            int selectedId = radioSexGroup.getCheckedRadioButtonId();
            radioButton = (RadioButton) findViewById(selectedId);
        }catch (Exception e){
            e.printStackTrace();
        }
        return radioButton.getText().toString();
    }
    public void validationSuccess() {
        getValidation.checkPasswordMatching(uPass,uConfPass);
        //Toast.makeText(this, ""+uName+" "+uGender+" "+uPass+" "+uConfPass, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void passwordMatched() {
        if(isNetworkAvailable()){
            pd.show();
            callThread();
        }else{
            Toast.makeText(this, "Please use Internet", Toast.LENGTH_SHORT).show();
        }
    }

    private void callThread() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //Al user Data got, Send to Firebase now
                    getRes = mGetPhoneNsetFire.setCustInfoFireDB(mGetPhoneNsetFire.getLocalPhone(),uName,uGender,uPass);
                    Thread.sleep(700);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(getRes){
                            try{
                                pd.dismiss();
                                Toast.makeText(context, "Account successfully created.", Toast.LENGTH_SHORT).show();
                                Intent signInAuth = new Intent(CreateUserAcc.this,SignInAct.class);
                                startActivity(signInAuth);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }else{
                            pd.dismiss();
                            Toast.makeText(context, "Failed, check internet and try again", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            }
        }).start();
    }

    @Override
    public void wrongPassMsg() {
        getUpass.setError("Password Length must be (4-12)");
        getUconfPass.setError("Password Length must be (4-12)");
        getUpass.requestFocus();
        getUconfPass.requestFocus();
    }

    @Override
    public void wrongPhoneMsg() {

    }

    @Override
    public void passMatchingMsg() {
        Toast.makeText(this, "Password must be same", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void wrongNameValidationMsg() {
        getUname.setError("Valid name is required");
        getUname.requestFocus();
    }

    private void initializePrimary() {
        handler = new Handler(getApplicationContext().getMainLooper());
        context = CreateUserAcc.this;
        database = FirebaseDatabase.getInstance();
        mySharedPref = getSharedPreferences(myRef, Context.MODE_PRIVATE);
        mGetPhoneNsetFire = new SendGetLocToFromFirebase(context,mySharedPref,database);
        pd = new ProgressDialog(CreateUserAcc.this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.setIndeterminate(true);
    }

    public boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager =(ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }
}
