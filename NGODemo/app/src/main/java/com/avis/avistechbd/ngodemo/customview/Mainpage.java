package com.avis.avistechbd.ngodemo.customview;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.avis.avistechbd.ngodemo.R;
import com.avis.avistechbd.ngodemo.ViewHandler.DialogBoxOpenUp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import backendservices.SendGetLocToFromFirebase;
import butterknife.ButterKnife;
import butterknife.OnClick;
import maps_section.MyMapsActivity;

public class Mainpage extends AppCompatActivity {
    private static final int REQUEST_LOCATION = 1;
    private Context context;
    private DialogBoxOpenUp showDialog;
    private FirebaseDatabase database;
    private SharedPreferences mySharedPref;
    private SendGetLocToFromFirebase mGetPhoneNsetFire;
    private String myRef = "myCustPhone"; //Reference_Key
    private ProgressDialog pd;
    private Handler handler;
    private SendGetLocToFromFirebase mChkUsersLoggin;
    String uPhn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainpage);
        ButterKnife.bind(this);
        handler = new Handler(Looper.getMainLooper());
        pd = new ProgressDialog(Mainpage.this);
        pd.setTitle("Customer Account");
        pd.setMessage("Loading...");
        pd.setIndeterminate(false);
        pd.setCancelable(false);
        context = Mainpage.this;
        showDialog = new DialogBoxOpenUp(context);
        database = FirebaseDatabase.getInstance();
        mySharedPref = getSharedPreferences(myRef, Context.MODE_PRIVATE);
        mGetPhoneNsetFire = new SendGetLocToFromFirebase(context,mySharedPref,database);
        mChkUsersLoggin = new SendGetLocToFromFirebase(context,mySharedPref,database);
    }

    @OnClick({R.id.u_sign_in, R.id.u_sign_up})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.u_sign_in:
                callSignInAct();
                break;
            case R.id.u_sign_up:
                callSignUpAct();
                break;
        }
    }

    private void callSignUpAct() {
        Intent getPhonAuth = new Intent(Mainpage.this,PhoneAuth.class);
        getPhonAuth.putExtra("debdash","SIGNUP");
        startActivity(getPhonAuth);
    }

    private void callSignInAct() {
        //test_if_user_exist?? [uLog == 1]
        pd.show();
        uPhn = mChkUsersLoggin.getLocalLoginPhone();
        if(isNetworkAvailable() && checkPermission()){
            if(uPhn.compareToIgnoreCase("+88017000000") != 0){
                checkUserLogIn();
            }else{
                pd.dismiss();
                Intent logAcc = new Intent(Mainpage.this,SignInAct.class);
                startActivity(logAcc);
            }
        }else {
            pd.dismiss();
            Toast.makeText(this, "Please use Internet", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        String dialMsg = "Do you want to Exit?";
        showDialog.createDialogBoxCancellation(dialMsg);
    }

    private void checkUserLogIn(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    final String uPhn = mChkUsersLoggin.getLocalLoginPhone();
                    DatabaseReference reference = database.getInstance().getReference();
                    Query query = reference.child(uPhn);
                    query.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                for (DataSnapshot issue : dataSnapshot.getChildren()) {
                                    if(issue.getKey().toString().equals("uLog") && issue.getValue().toString().equals("1")){
                                        Log.d("snap uLog"," "+issue.getValue().toString());
                                        try {
                                            Thread.sleep(200);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                pd.dismiss();
                                                Intent directLogAcc = new Intent(Mainpage.this, MyMapsActivity.class);
                                                directLogAcc.putExtra("phone",uPhn);
                                                startActivity(directLogAcc);
                                                Log.d("snap 1","direct Handler");
                                            }
                                        });
                                        break;
                                    }else if(issue.getKey().toString().equals("uLog") && issue.getValue().toString().equals("0")){
                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                pd.dismiss();
                                                Intent logAcc = new Intent(Mainpage.this,SignInAct.class);
                                                startActivity(logAcc);
                                                Log.d("snap 0","Log in please");
                                            }
                                        });
                                        break;
                                    }else{
                                        Log.d("snap"," "+issue.getValue().toString()+" issue "+issue);
                                    }
                                }
                            }
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            pd.dismiss();
                            Toast.makeText(context, "Try Signin", Toast.LENGTH_SHORT).show();
                        }
                    });
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager =(ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    private boolean checkPermission(){
        boolean flagChk = false;
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                flagChk = false;
                callForRequestPermission();
            }else{
                flagChk = true;
            }
        } catch (Exception e) {
            Toast.makeText(this, R.string.location_manager_init_error, Toast.LENGTH_SHORT).show();
        }
        return flagChk;
    }
    private void callForRequestPermission() {
        ActivityCompat.requestPermissions(Mainpage.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
    }
}
