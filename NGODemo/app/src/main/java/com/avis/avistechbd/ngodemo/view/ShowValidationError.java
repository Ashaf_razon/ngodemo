package com.avis.avistechbd.ngodemo.view;

public interface ShowValidationError {
    void wrongPassMsg();
    void wrongPhoneMsg();
    void wrongNameValidationMsg();
    void passMatchingMsg();
    void passwordMatched();
}
