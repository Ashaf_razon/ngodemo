package com.avis.avistechbd.ngodemo.ViewHandler;
import android.app.Activity;
import android.content.Context;
import com.gdacciaro.iOSDialog.iOSDialog;
import com.gdacciaro.iOSDialog.iOSDialogBuilder;
import com.gdacciaro.iOSDialog.iOSDialogClickListener;

public class DialogBoxOpenUp {
    Context context;
    String dialogWarning = null;
    public DialogBoxOpenUp(Context context) {
        this.context = context;
    }

    public void createDialogBoxCancellation(String msg) {
        this.dialogWarning = msg;
        new iOSDialogBuilder(context)
                .setTitle("Alert")
                .setSubtitle("Do you want to exit?")
                .setBoldPositiveLabel(true)
                .setCancelable(false)
                .setPositiveListener("Yes",new iOSDialogClickListener() {
                    @Override
                    public void onClick(iOSDialog dialog) {
                        ((Activity)context).finish();
                        dialog.dismiss();
                    }
                })
                .setNegativeListener("Cancle", new iOSDialogClickListener() {
                    @Override
                    public void onClick(iOSDialog dialog) {
                        dialog.dismiss();
                    }
                })
                .build().show();
    }
}
