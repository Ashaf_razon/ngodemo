package com.avis.avistechbd.ngodemo.model;

import com.avis.avistechbd.ngodemo.presenter.UserValidationInterface;
import com.avis.avistechbd.ngodemo.view.ShowValidationError;

public class Vaidiation implements UserValidationInterface{

    private ShowValidationError setValidationEr;
    public Vaidiation(ShowValidationError setValidationEr) {
        this.setValidationEr = setValidationEr;
    }

    @Override
    public boolean singlePhnValidation(String getUphn) {
        boolean flagPhn = true;
        if(getUphn.isEmpty() || getUphn.length()< 10 || getUphn.charAt(0) != '1' ||
                (getUphn.charAt(1) >= '0' && getUphn.charAt(1) <='4')){
            flagPhn = false;
            setValidationEr.wrongPhoneMsg();
        }
        return flagPhn;
    }

    @Override
    public boolean singlePassValidation(String getUpass) {
        boolean flagPass = true;
        if(getUpass.length()<4){
            flagPass = false;
            setValidationEr.wrongPassMsg();
        }
        return flagPass;
    }

    @Override
    public boolean singleNameValidation(String getUname) {
        boolean flagName = true;
        if(getUname.isEmpty() || getUname.length() < 4){
            flagName = false;
            setValidationEr.wrongNameValidationMsg();
        }
        return flagName;
    }

    @Override
    public void checkPasswordMatching(String pass, String confPass) {
        if(pass.compareTo(confPass) != 0){
            setValidationEr.passMatchingMsg();
        }else{
            setValidationEr.passwordMatched();
        }
    }
}
