package com.avis.avistechbd.ngodemo.customview;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.avis.avistechbd.ngodemo.R;
import com.avis.avistechbd.ngodemo.model.Vaidiation;
import com.avis.avistechbd.ngodemo.presenter.UserValidationInterface;
import com.avis.avistechbd.ngodemo.view.ShowValidationError;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import backendservices.SendGetLocToFromFirebase;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import maps_section.MyMapsActivity;

public class SignInAct extends AppCompatActivity implements ShowValidationError{

    private static final int REQUEST_LOCATION = 1;

    @BindView(R.id.getSignedUphone)
    EditText getSignedUphone;
    @BindView(R.id.getSignedUpass)
    EditText getSignedUpass;
    @BindView(R.id.forgoUpass)
    TextView forgoUpass;
    @BindView(R.id.createNewUacc)
    TextView createNewUacc;
    @BindView(R.id.getUSignedSubmit)
    Button getUSignedSubmit;

    private UserValidationInterface getValidation;
    private String getUphn;
    private String getUpass;
    private Context context;
    private FirebaseDatabase database;
    int flagLog;
    private SharedPreferences mySharedPref;
    private String myRef = "myCustPhone"; //Reference_Key

    private SharedPreferences.Editor editSave;//LoggedIn_phn
    private String myPhnKeyLogged = "custPhnLogged";//Logged_in_Phn
    private SendGetLocToFromFirebase mSetuLogmsg;
    private ProgressDialog pd;
    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
        forgoUpass.setPaintFlags(forgoUpass.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        createNewUacc.setPaintFlags(createNewUacc.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        getValidation = new Vaidiation( SignInAct.this);
        handler = new Handler(Looper.getMainLooper());
        context = SignInAct.this;
        database = FirebaseDatabase.getInstance();
        mySharedPref = getSharedPreferences(myRef, Context.MODE_PRIVATE);
        mSetuLogmsg = new SendGetLocToFromFirebase(context,mySharedPref,database);

        pd = new ProgressDialog(SignInAct.this);
        pd.setTitle("User Logging");
        pd.setMessage("Loading");
        pd.setCancelable(false);
        pd.setIndeterminate(false);
    }

    @OnClick(R.id.getUSignedSubmit)
    public void onViewClicked() {
        try{
            getUphn = getSignedUphone.getText().toString().replaceAll("\\s+","");
            getUpass = getSignedUpass.getText().toString().replaceAll("\\s+","");
            if(getValidation.singlePhnValidation(getUphn)&& getValidation.singlePassValidation(getUpass)){
                if(isNetworkAvailable() && checkPermission()){
                    validationSuccess();
                }else{
                    Toast.makeText(SignInAct.this, "Please use Internet and enable permission", Toast.LENGTH_SHORT).show();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @OnClick({R.id.forgoUpass, R.id.createNewUacc})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.forgoUpass:
                callForgotPassActiv();
                break;
            case R.id.createNewUacc:
                callCreateNewAcc();
                break;
        }
    }

    private void callCreateNewAcc() {
        Intent getPhnAuth = new Intent(SignInAct.this,PhoneAuth.class);
        startActivity(getPhnAuth);
    }

    private void callForgotPassActiv() {
        Intent getPassBackAuth = new Intent(SignInAct.this,PhoneAuth.class);
        getPassBackAuth.putExtra("debdash","FORGOTPASS");
        startActivity(getPassBackAuth);
    }

    @Override
    public void wrongPassMsg() {
        getSignedUpass.setError("Password Length must be (4-12)");
        getSignedUpass.requestFocus();
    }

    @Override
    public void wrongPhoneMsg() {
        getSignedUphone.setError("Valid number is required");
        getSignedUphone.requestFocus();
    }

    public void validationSuccess() {
        flagLog = 0;
        pd.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    DatabaseReference reference = database.getInstance().getReference();
                    Query query = reference.child("+880"+getUphn);
                    query.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                for (DataSnapshot issue : dataSnapshot.getChildren()) {
                                    if(issue.getKey().toString().equals("ePass") && issue.getValue().toString().equals(getUpass)){
                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                try{
                                                    if(mSetuLogmsg.setLoginDB("+880"+getUphn)){
                                                        setCustomerLoggedPhoneDB("+880"+getUphn);
                                                        Toast.makeText(SignInAct.this, "Successfully Logged in", Toast.LENGTH_SHORT).show();
                                                    }
                                                    pd.dismiss();
                                                    Intent showMap = new Intent(SignInAct.this,MyMapsActivity.class);
                                                    showMap.putExtra("phone",getUphn);
                                                    startActivity(showMap);
                                                }catch(Exception e){
                                                    e.printStackTrace();
                                                    pd.dismiss();
                                                }
                                            }
                                        });
                                    }else if(issue.getKey().toString().equals("ePass") && issue.getValue().toString().equals(getUpass) == false){
                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                try{
                                                    Toast.makeText(SignInAct.this, "Incorrect Password", Toast.LENGTH_SHORT).show();
                                                    pd.dismiss();
                                                }catch(Exception e){
                                                    e.printStackTrace();
                                                    pd.dismiss();
                                                }
                                            }
                                        });
                                    }
                                }
                            }else{
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        try{
                                            Toast.makeText(SignInAct.this, "User not exist, Please Signup", Toast.LENGTH_SHORT).show();
                                            Intent goSignUp = new Intent(SignInAct.this,PhoneAuth.class);
                                            startActivity(goSignUp);
                                            pd.dismiss();
                                        }catch(Exception e){
                                            e.printStackTrace();
                                            pd.dismiss();
                                        }
                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            pd.dismiss();
                            Toast.makeText(SignInAct.this, "SignIn DB Error, Try again latter ", Toast.LENGTH_SHORT).show();
                        }
                    });
                    Thread.sleep(500);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }).start();
        Log.d("Tag","End");
    }

    @Override
    public void wrongNameValidationMsg() {}

    @Override
    public void passMatchingMsg() {}

    @Override
    public void passwordMatched() {
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }

    public boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager =(ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    private void callForRequestPermission() {
        ActivityCompat.requestPermissions(SignInAct.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
    }

    private boolean checkPermission(){
        boolean flagChk = false;
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                flagChk = false;
                callForRequestPermission();
            }else{
                flagChk = true;
            }
        } catch (Exception e) {
            Toast.makeText(this, R.string.location_manager_init_error, Toast.LENGTH_SHORT).show();
        }
        return flagChk;
    }

    private void setCustomerLoggedPhoneDB(String setCustPhone) {
        try{
            editSave = mySharedPref.edit();
            editSave.putString(myPhnKeyLogged,setCustPhone);
            editSave.commit();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
