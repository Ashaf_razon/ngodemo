package com.avis.avistechbd.ngodemo.customview;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.avis.avistechbd.ngodemo.R;
import com.avis.avistechbd.ngodemo.model.Vaidiation;
import com.avis.avistechbd.ngodemo.presenter.UserValidationInterface;
import com.avis.avistechbd.ngodemo.view.ShowValidationError;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class PhoneAuth extends AppCompatActivity implements ShowValidationError {

    @BindView(R.id.enterPhoneAuth)
    EditText enterPhoneAuth;
    @BindView(R.id.codeSendInPhnText)
    TextView codeSendInPhnText;

    @BindView(R.id.getAuthCode_1)
    EditText getAuthCode1;
    @BindView(R.id.getAuthCode_2)
    EditText getAuthCode2;
    @BindView(R.id.getAuthCode_3)
    EditText getAuthCode3;
    @BindView(R.id.getAuthCode_4)
    EditText getAuthCode4;
    @BindView(R.id.getAuthCode_5)
    EditText getAuthCode5;
    @BindView(R.id.getAuthCode_6)
    EditText getAuthCode6;
    @BindView(R.id.submit_code)
    Button submitCode;

    private UserValidationInterface getValidation;
    private String getUphn;
    //Authentication
    private ProgressDialog pd;
    private FirebaseAuth mAuth;
    private String sentCode;
    //UsersPhoneSave
    private SharedPreferences mySharedPref;
    private SharedPreferences.Editor editSave;
    private String myRef = "myCustPhone"; //Reference_Key
    private String myPhnKey = "custPhn";
    boolean flagReg ;
    boolean flagReg2;
    //FireDB_instance
    private FirebaseDatabase database;
    private String getFlagsAct;
    private Handler handler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_auth);
        ButterKnife.bind(this);
        initPrimarysettings();
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            textAutoShift();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        }).start();
    }

    private void textAutoShift() {


        getAuthCode1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(getAuthCode1.getText().toString().length()== 1){
                    getAuthCode1.clearFocus();
                    getAuthCode2.requestFocus();
                    getAuthCode2.setCursorVisible(true);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        getAuthCode2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(getAuthCode2.getText().toString().length() == 1){
                    getAuthCode2.clearFocus();
                    getAuthCode3.requestFocus();
                    getAuthCode3.setCursorVisible(true);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        getAuthCode3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(getAuthCode3.getText().toString().length() == 1){
                    getAuthCode3.clearFocus();
                    getAuthCode4.requestFocus();
                    getAuthCode4.setCursorVisible(true);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        getAuthCode4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(getAuthCode4.getText().toString().length() == 1){
                    getAuthCode4.clearFocus();
                    getAuthCode5.requestFocus();
                    getAuthCode5.setCursorVisible(true);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        getAuthCode5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(getAuthCode5.getText().toString().length() == 1){
                    getAuthCode5.clearFocus();
                    getAuthCode6.requestFocus();
                    getAuthCode6.setCursorVisible(true);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void initPrimarysettings() {
        try{
            handler = new Handler(Looper.getMainLooper());
            getFlagsAct = getIntent().getStringExtra("debdash");
            getValidation = new Vaidiation(PhoneAuth.this);
            mAuth = FirebaseAuth.getInstance();
            mySharedPref = getSharedPreferences(myRef, Context.MODE_PRIVATE);
            database = FirebaseDatabase.getInstance();
            pd = new ProgressDialog(PhoneAuth.this);
            pd.setMessage("Loading...");
            pd.setCancelable(false);
            pd.setIndeterminate(true);
        }catch (Exception e){
            e.printStackTrace();
        }
    }



    @OnClick({R.id.varifyPhonAuth, R.id.submit_code})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.varifyPhonAuth:
                if(isNetworkAvailable()){
                    getValidationNsendForAuth(); //getting Validation
                    codeSendInPhnText.setText(""); //sending phone auth Code
                }else{
                    Toast.makeText(this, "Please use Internet", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.submit_code:
                if(isNetworkAvailable()){
                    if (checkCodeField()) { //code field checking if not empty
                        varifyCode(getUsersCode()); // phone auth code varification
                    } else {
                        Toast.makeText(this, "All code must be entered", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    private boolean checkCodeField() {
        boolean fieldFlag = false;
        try {
            if (getAuthCode1.getText().toString() != null && getAuthCode2.getText().toString() != null && getAuthCode3.getText().toString() != null
                    && getAuthCode4.getText().toString() != null && getAuthCode5.getText().toString() != null && getAuthCode6.getText().toString() != null) {
                fieldFlag = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fieldFlag;
    }

    private void getValidationNsendForAuth() {
        try {
            getUphn = enterPhoneAuth.getText().toString().replaceAll("\\s+", "");
            if (getValidation.singlePhnValidation(getUphn)) {
                validationSuccess();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void wrongPassMsg() {

    }

    @Override
    public void wrongPhoneMsg() {
        enterPhoneAuth.setError("Valid number is required");
        enterPhoneAuth.requestFocus();
    }

    @Override
    public void wrongNameValidationMsg() {
    }

    @Override
    public void passMatchingMsg() {
    }

    @Override
    public void passwordMatched() {

    }

    public void validationSuccess() {
        try {
            checkIfUserExist();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkIfUserExist() {
        pd.setTitle("Code send");
        pd.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                DatabaseReference reference = database.getInstance().getReference();
                Query query = reference.child("+880"+getUphn);
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if(getFlagsAct.compareToIgnoreCase("SIGNUP") == 0){
                                        Intent goSignInAct = new Intent(PhoneAuth.this,SignInAct.class);
                                        pd.dismiss();
                                        Toast.makeText(PhoneAuth.this, "User already exist, try Login", Toast.LENGTH_SHORT).show();
                                        finish();
                                        startActivity(goSignInAct);
                                    }else{
                                        sendVarificationCode();
                                    }
                                }
                            });
                        }else{
                            //getVar Code
                            sendVarificationCode();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        pd.dismiss();
                        Toast.makeText(PhoneAuth.this, "Auth DB Error, Try again latter ", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).start();
    }

    private void sendVarificationCode() {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                "+880" + getUphn,   // Phone number to verify
                60,              // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,        // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
    }

    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String setVarCode = phoneAuthCredential.getSmsCode();
            dismissProBar();
            if (setVarCode != null) {
                setUsersCode(setVarCode);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            dismissProBar();
            Toast.makeText(PhoneAuth.this, "Try again, verification Failed " + e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            sentCode = s;
            submitCode.setEnabled(true);
            submitCode.setFocusable(true);
            codeSendInPhnText.setText("Enter the code, that was sent to  +880" + getUphn);
            dismissProBar();
        }
    };

    private void varifyCode(String uSetCode) {
        if (uSetCode.isEmpty() == false) {
            pd.setTitle("Code check");
            pd.setCancelable(false);
            pd.show();
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(sentCode, uSetCode);
            signInWithPhoneAuthCredential(credential);
        } else {
            Toast.makeText(this, "All code must be entered", Toast.LENGTH_SHORT).show();
        }
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            try{
                                setCustomerPhoneDB("+880"+getUphn);
                                dismissProBar();
                                if(getFlagsAct.compareToIgnoreCase("SIGNUP") == 0){
                                    Intent createNewAcc = new Intent(PhoneAuth.this,CreateUserAcc.class);
                                    finish();
                                    startActivity(createNewAcc);
                                }else if(getFlagsAct.compareToIgnoreCase("FORGOTPASS") == 0){
                                    Intent getPassBackAuth = new Intent(PhoneAuth.this,ForgotPass.class);
                                    finish();
                                    startActivity(getPassBackAuth);
                                }else{
                                    Toast.makeText(PhoneAuth.this, "Something is wrong", Toast.LENGTH_LONG).show();
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        } else {
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                dismissProBar();
                                Toast.makeText(PhoneAuth.this, "Verification Code invalid", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
    }

    private String getUsersCode() {
        String getCode = "";
        try {
            getCode += getAuthCode1.getText().toString();
            getCode += getAuthCode2.getText().toString();
            getCode += getAuthCode3.getText().toString();
            getCode += getAuthCode4.getText().toString();
            getCode += getAuthCode5.getText().toString();
            getCode += getAuthCode6.getText().toString();
        } catch (Exception e) {
            Toast.makeText(this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return getCode;
    }

    private void setUsersCode(String setUcode) {
        try {
            getAuthCode1.setText(setUcode.charAt(0));
            getAuthCode1.setText(setUcode.charAt(1));
            getAuthCode1.setText(setUcode.charAt(2));
            getAuthCode1.setText(setUcode.charAt(3));
            getAuthCode1.setText(setUcode.charAt(4));
            getAuthCode1.setText(setUcode.charAt(5));
            //Toast.makeText(this, "setCode: " + setUcode, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
            //Toast.makeText(this, "get Code" + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void dismissProBar() {
        if (pd.isShowing())
            pd.dismiss();
    }

    public boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager =(ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public void setCustomerPhoneDB(String setCustPhone) {
        try{
            editSave = mySharedPref.edit();
            editSave.putString(myPhnKey,setCustPhone);
            //Toast.makeText(this, "Phone Saved in Local DB "+setCustPhone, Toast.LENGTH_SHORT).show();
            editSave.commit();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        //finish();
    }
}
