package com.avis.avistechbd.ngodemo.presenter;

public interface UserValidationInterface {
    boolean singlePhnValidation(String getUphn);
    boolean singlePassValidation(String getUpass);
    boolean singleNameValidation(String getUname);
    void checkPasswordMatching(String pass, String confPass);
}
