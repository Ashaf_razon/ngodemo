package maps_section;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.avis.avistechbd.ngodemo.R;
import com.avis.avistechbd.ngodemo.customview.Mainpage;
import com.gdacciaro.iOSDialog.iOSDialog;
import com.gdacciaro.iOSDialog.iOSDialogBuilder;
import com.gdacciaro.iOSDialog.iOSDialogClickListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import backendservices.SendGetLocToFromFirebase;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import maps_backend.CheckUserPermission;
import maps_backend.GetLocation;
import maps_backend.GetSetUserDataToLocNfirebase;

public class MyMapsActivity extends AppCompatActivity implements OnMapReadyCallback, GetLocation {
    /*
        1) Get isNetowrkAvailabe
        2) Get Common [_Loc_]Permission
        3) Get GPS Permission
        4) Call GetLocation
        5) Call onLocation Change UpdateTime: 2000ms
        6) onSpinner Item Click -> Thread Call to Load &  marker set (View portion for Handler)
            a)getAllWantedWorker calls 2 times [1 for get ready 2 for get all data in it]
            b)call marker set
        7) get Permission 3 times Check [OnCreate Method]

    */
    //Constant
    private static final int REQUEST_LOCATION = 1;
    private static final int REQUEST_PHONE_CALL = 1;
    private String city,area;
    //maps_VIEW
    @BindView(R.id.mySearchServ)
    Spinner searchServ;
    @BindView(R.id.mySearchMapFree)
    AppCompatImageView searchMapFree;
    @BindView(R.id.upArraw)
    ImageView upArraw;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.u_detection)
    ImageView uDetection;
    private SupportMapFragment mapFragment;
    @BindView(R.id.chargeSearv)
    TextView chargeView;
    @BindView(R.id.ratingServ)
    TextView ratingView;
    @BindView(R.id.addrsSearv)
    TextView addrsView;
    private LocationManager locationManager;

    //temp_VIEW
    //private String lattitude = "23.750202", longitude = "90.392685";
    private String add = null;

    //MultiMarker
    private MarkerOptions options = new MarkerOptions();
    //Arraylist_will_be_init_Latter
    private ArrayList<String> latAr;
    private ArrayList<String> lngAr;
    private ArrayList<String> phnNoServAr;
    private ArrayList<String> calPhn;
    private ArrayList<String> servCost;
    private int lisTServ = 0;
    private String mobLieNoGet = "01717000000";
    private Marker myMarker;
    //BottomSheet
    BottomSheetBehavior behavior;

    String[] workType = {"Select your worker", "Washroom Wash", "Electrician", "Water Problem", "House Helping Hand"};
    //String wType = "Washroom Wash";
    //Control_Spinner_Flag
    int arrWCl = 2;
    int flagLocCall = 0;

    //Context
    private Context context;
    //ByDefault -> company position
    private String uLatt = "23.750202";
    private String uLong = "90.392685";
    //Customer_Local_with_KEY
    private SharedPreferences mySharedPref, myWorkShared;
    String workKey = "work";//spinner_work_set_key
    private String myRef = "myCustRef"; //Customer_Loc_Ref_Key->Get_All_Local_Values
    //FireDB_instance
    FirebaseDatabase database;
    //getUserLocal_Info_Interface_instance
    private GetSetUserDataToLocNfirebase getMyAllLoc; //class_Object
    private CheckUserPermission getMyPermission; //Permission_Check
    private Handler handler;
    private SendGetLocToFromFirebase mSetFireLogOut;
    private SharedPreferences.Editor editSave;//LoggedIn_phn
    private String myPhnKeyLogged = "custPhnLogged";//Logged_in_Phn
    private String getLogPhnIntent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_maps_activity);
        ButterKnife.bind(this);
        //getting_this_application_context
        context = MyMapsActivity.this;
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(View.INVISIBLE);
        //getPhn
        getLogPhnIntent = getIntent().getStringExtra("phone");
        //DB_get_set
        database = FirebaseDatabase.getInstance();
        myWorkShared = getSharedPreferences(workKey, Context.MODE_PRIVATE);
        mySharedPref = getSharedPreferences(myRef, Context.MODE_PRIVATE);
        handler = new Handler(getApplicationContext().getMainLooper());
        mSetFireLogOut = new SendGetLocToFromFirebase(context,mySharedPref,database);
        //User_Local_info_init
        arrayListInit();
        initLocationManagerPermission();
        initMapView();//MapView
        bottomSheetInit();
        //init_Map_Views_element
        initSpinnSearch();
        //initially_users_Location_view
        getPermissionInfoNLocation();
    }

    private void getPermissionInfoNLocation() {
        if (getMyPermission.checkGPSpermission()) {
            //Done_Getting_All_Permission_(Location & GPS)
            try {
                if (getMyPermission.isNetworkAvailable()) {
                    //Get_Location_Once
                    getMyAllLoc.getLocation();
                } else {
                    Toast.makeText(this, R.string.internet_msg, Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, R.string.location_fetch_error, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, R.string.gps_msg, Toast.LENGTH_SHORT).show();
        }
    }

    private void initLocationManagerPermission() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                callForRequestPermission();
                //return;
            }
        } catch (Exception e) {
            Toast.makeText(this, R.string.location_manager_init_error, Toast.LENGTH_SHORT).show();
        }
        //get_Location_&_Permission_instance
        getMyAllLoc = new GetSetUserDataToLocNfirebase(locationManager, context, (Activity) context, MyMapsActivity.this);
        getMyPermission = new CheckUserPermission(context, (Activity) context, locationManager);
    }

    @Override
    public void getLocationOnce(String myLatt, String myLong) {
        this.uLatt = myLatt;
        this.uLong = myLong;
        SendGetLocToFromFirebase mySendGetFire = new SendGetLocToFromFirebase(context, mySharedPref, database);
        try {
            //auto usersLoc Update removed
            if (flagLocCall == 0) {
                //initial_map_show
                showOnMap(Double.parseDouble(myLatt), Double.parseDouble(myLong), null, null, null);
            } else {
                //map_with_Worker_location
                //get_all_array_List[Use_Thread]
                if (getSavedWorkd() != null) {
                    myThreadMap();
                } else {
                    Toast.makeText(this, R.string.no_worker_selection_msg, Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Error_Firebase_Calling", Toast.LENGTH_SHORT).show();
        }
    }

    private void callForRequestPermission() {
        //ActivityCompat.requestPermissions(MyMapsActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
    }

    private void initMapView() {
        try {
            mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.my_map);
            if (mapFragment != null) {
                mapFragment.getMapAsync(this);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void bottomSheetInit() {
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.myMainMapLay);
        View bottomSheet = coordinatorLayout.findViewById(R.id.bottom_sheet2);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        arrWCl = 3;
                        upArraw.setImageDrawable(getResources().getDrawable(R.drawable.ic_dwn_arr));
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        arrWCl = 2;
                        upArraw.setImageDrawable(getResources().getDrawable(R.drawable.ic_upr_arr));
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        Button button = (Button) findViewById(R.id.callServe);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                if(mobLieNoGet.toString().equals("01717000000") != true)
                    toServPhoneCallEnabe();
                else
                    Toast.makeText(MyMapsActivity.this, "Please select worker", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void toServPhoneCallEnabe() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        try{
            callIntent.setData(Uri.parse("tel:" + mobLieNoGet));
        }catch (Exception e){
            e.printStackTrace();
        }

        if (ActivityCompat.checkSelfPermission(MyMapsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MyMapsActivity.this, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
            return;
        }
        startActivity(callIntent);
    }

    private void initSpinnSearch() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(MyMapsActivity.this, android.R.layout.simple_spinner_item, workType);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        searchServ.setAdapter(dataAdapter);
        searchServ.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    flagLocCall = 0;
                    if (position >= 1) {
                        flagLocCall = 1;
                        String item = parent.getItemAtPosition(position).toString();
                        setUserWork(item);
                        progressBar.setVisibility(View.VISIBLE);
                        myThreadMap();
                    } else {
                        flagLocCall = 0;
                        Toast.makeText(MyMapsActivity.this, R.string.select_worker_warning, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(MyMapsActivity.this, R.string.select_spinner_error, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                try {
                    flagLocCall = 0;
                    Toast.makeText(MyMapsActivity.this, R.string.no_worker_selection_msg, Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Toast.makeText(MyMapsActivity.this, R.string.no_item_spinner_selection, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void myThreadMap() {
        new Thread(new Runnable() {
            int i = 0;
            int progressStatus = 0;
            //boolean waitForBar = false;
            @Override
            public void run() {
                try {
                    //[1]
                    getAllWantedServer();
                    while (progressStatus < 100) {
                        progressStatus += doWork();
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        handler.post(new Runnable() {
                            public void run() {
                                if (progressStatus < 100) {
                                    progressBar.setProgress(progressStatus);
                                    i++;
                                } else {
                                    //[2]
                                    getAllWantedServer();
                                    showOnMap(Double.parseDouble(uLatt), Double.parseDouble(uLong), latAr, lngAr, phnNoServAr);
                                    progressBar.setVisibility(View.GONE);
                                }
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            private int doWork() {
                return i * 3;
            }
        }).start();
    }

    @OnClick(R.id.upArraw)
    public void onUpArrawClicked() {
        if (arrWCl % 2 == 0) {
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            upArraw.setImageDrawable(getResources().getDrawable(R.drawable.ic_dwn_arr));
            arrWCl = 3;
        } else if (arrWCl % 2 == 1) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            upArraw.setImageDrawable(getResources().getDrawable(R.drawable.ic_upr_arr));
            arrWCl = 2;
        }
    }

    private void setUserWork(String setWork) {
        try {
            SharedPreferences.Editor editSave = myWorkShared.edit();
            editSave.putString(workKey, setWork);
            editSave.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getSavedWorkd() {
        String wGet = "";
        try {
            wGet = myWorkShared.getString(workKey, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return wGet;
    }

    private void showOnMap(final double lt, final double lng, final ArrayList<String> latAr, final ArrayList<String> lngAr, final ArrayList<String> phnNoServAr) {
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                try {
                    try {
                        googleMap.clear();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //init_map_MARKER_view
                    try {
                        if (flagLocCall == 0) {
                            setCustomerMarker(options, googleMap, lt, lng);
                        } else {
                            //map_view_with_worker
                            if (phnNoServAr.isEmpty()) {
                                setCustomerMarker(options, googleMap, lt, lng);
                                Toast.makeText(MyMapsActivity.this, R.string.no_worker_found, Toast.LENGTH_LONG).show();
                            } else {
                                for (int x = 0; x <= phnNoServAr.size(); x++) {
                                    if (x < phnNoServAr.size()) {
                                        options.position(new LatLng(Double.parseDouble(latAr.get(x)), Double.parseDouble(lngAr.get(x))));
                                        options.title(phnNoServAr.get(x));
                                        options.snippet("Job: " + getSavedWorkd());
                                        options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
                                    } else {
                                        options.position(new LatLng(lt, lng));
                                        options.title("My Position");
                                        options.snippet("Customer");
                                        options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                                    }
                                    myMarker = googleMap.addMarker(options);
                                    if (x < phnNoServAr.size()) {
                                        myMarker.setTag(new LatLng(Double.parseDouble(latAr.get(x)), Double.parseDouble(lngAr.get(x))));
                                    } else {
                                        myMarker.setTag(new LatLng(lt, lng));
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {
                            markerClickMethod(marker);
                            return false;
                        }
                    });
                } catch (Exception e) {
                    Toast.makeText(MyMapsActivity.this, "Error Multiple Marker : " + lisTServ, Toast.LENGTH_LONG).show();
                }
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lt, lng), 15));
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                googleMap.getUiSettings().setZoomControlsEnabled(true);
                googleMap.getUiSettings().setZoomGesturesEnabled(true);
                googleMap.getUiSettings().setCompassEnabled(true);
                //googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                googleMap.getUiSettings().setRotateGesturesEnabled(true);
            }
        });
    }

    private void setCustomerMarker(MarkerOptions options, GoogleMap googleMap, double lt, double lng) {
        options.position(new LatLng(lt, lng));
        options.title("My Position");
        options.snippet("Customer");
        options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        myMarker = googleMap.addMarker(options);
        myMarker.setTag(new LatLng(lt, lng));
    }

    public void markerClickMethod(Marker marker) {
        try {
            LatLng getLatLngMarker = marker.getPosition();
            double mrLt = getLatLngMarker.latitude;
            double mrLn = getLatLngMarker.longitude;
            String mrGetTitle = marker.getTitle();
            int flagGetMarker;
            if (mrGetTitle.compareToIgnoreCase("My Position") != 0) {
                uDetection.setImageDrawable(getResources().getDrawable(R.drawable.custom));
                flagGetMarker = phnNoServAr.indexOf(mrGetTitle);
                mobLieNoGet = calPhn.get(flagGetMarker);
                findMyAddress(mrLt, mrLn);
                chargeView.setText("Searvice Charge: " + servCost.get(flagGetMarker));
                chargeView.setTextColor(Color.parseColor("#E6F324"));
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            } else {
                //do_func_forCustomer_side
                uDetection.setImageDrawable(getResources().getDrawable(R.mipmap.customer_flag));
                chargeView.setText("User type: Customer");
                mobLieNoGet = "01717000000";
                chargeView.setTextColor(Color.parseColor("#54cb61"));
                findMyAddress(mrLt, mrLn);
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
            ratingView.setText("Rating: 4.3");
        } catch (Exception e) {
            ratingView.setText("No Location");
        }
    }

    private void findMyAddress(final double lt, final double lng) {
        Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
        List<Address> addresses;
        //ad1,ad2,ad3,ad4,ad5,ad6,ad7,ad8,ad9,ad10,ad11,ad12,ad13;
        try {
            addresses = gcd.getFromLocation(lt, lng, 1);
            //add = addresses.get(0).getAddressLine(0); // If any additional address line present then only, check with max available address lines by getMaxAddressLineIndex()
            city = addresses.get(0).getLocality();
            area = addresses.get(0).getFeatureName();
            add = area+", "+city+", Bangladesh";
            addrsView.setText("Address: " + add);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

    private void emptyArraylistField() {
        try {
            if (!latAr.isEmpty()) {
                latAr.clear();
            }
            if (!lngAr.isEmpty()) {
                lngAr.clear();
            }
            if (!phnNoServAr.isEmpty()) {
                phnNoServAr.clear();
            }
            if (!calPhn.isEmpty()) {
                calPhn.clear();
            }
            if (!servCost.isEmpty()) {
                servCost.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
            //Toast.makeText(context, "Unable Empty ArrayList -> " + latAr.size() + " " + lngAr.size() + " " + phnNoServAr.size() + " " + calPhn.size(), Toast.LENGTH_SHORT).show();
        }

    }

    private void arrayListInit() {
        latAr = new ArrayList<>();
        lngAr = new ArrayList<>();
        phnNoServAr = new ArrayList<>();
        calPhn = new ArrayList<>();
        servCost = new ArrayList<>();
    }

    public void getAllWantedServer() {
        DatabaseReference myRef2 = database.getReference();
        myRef2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    emptyArraylistField();
                    for (DataSnapshot snap : dataSnapshot.getChildren()) {
                        String chk;
                        if (snap != null && snap.child("eJob").getValue() != null) {
                            chk = String.valueOf(snap.child("eJob").getValue());//Getting_Job_Value//
                            if (chk.compareToIgnoreCase(getSavedWorkd()) == 0) {
                                try {
                                    latAr.add(snap.child("uLatt").getValue().toString());
                                    lngAr.add(snap.child("uLong").getValue().toString());
                                    phnNoServAr.add("Mobile No. " + snap.getKey().toString());
                                    servCost.add(snap.child("sCost").getValue().toString());
                                    calPhn.add(snap.getKey().toString());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    Log.e("dataChange", "Database isn't Ready" + e.getMessage());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(context, "DB system error", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        new iOSDialogBuilder(context)
                .setTitle("Alert")
                .setSubtitle("Do you want to Sign out ?")
                .setBoldPositiveLabel(true)
                .setCancelable(false)
                .setPositiveListener("Yes",new iOSDialogClickListener() {
                    @Override
                    public void onClick(iOSDialog dialog) {
                        mSetFireLogOut.setLogOutDB(getLogPhnIntent);
                        setCustomerLoggedPhoneDB("+88017000000");
                        finish();
                        dialog.dismiss();
                    }
                })
                .setNegativeListener("Not now", new iOSDialogClickListener() {
                    @Override
                    public void onClick(iOSDialog dialog) {
                        finish();
                        dialog.dismiss();
                    }
                })
                .build().show();
    }

    private void setCustomerLoggedPhoneDB(String setCustPhone) {
        try{
            editSave = mySharedPref.edit();
            editSave.putString(myPhnKeyLogged,setCustPhone);
            editSave.commit();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
