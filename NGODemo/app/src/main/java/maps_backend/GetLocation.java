package maps_backend;

public interface GetLocation {
    void getLocationOnce(String myLatt, String myLong);
}
