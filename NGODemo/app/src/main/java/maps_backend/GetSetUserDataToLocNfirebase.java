package maps_backend;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;
import com.avis.avistechbd.ngodemo.R;

public class GetSetUserDataToLocNfirebase implements LocationListener {
    /*
        )OnMapCallBack
        )LocationListener for UpdateLocation
        )ReSend Update LattLong to the parent
    */

    //Get_App_Context
    private Context context;
    //Get_App_Activity
    private Activity getActivity;
    private GetLocation getMyLocation;
    //get_Location
    private LocationManager locationManager;
    //Constant
    private static final int REQUEST_LOCATION = 1;
    private static final int MIN_UPDATE_TIME = 2000;
    private static final int MIN_UPDATE_DISTANCE = 0;

    public GetSetUserDataToLocNfirebase(LocationManager locationManager,Context context,Activity getActivity,GetLocation getMyLocation) {
        this.context = context;
        this.getActivity = getActivity;
        this.getMyLocation = getMyLocation;
        this.locationManager = locationManager;
    }


    public void getLocation() {
        double latti = 0.0;
        double longi = 0.0;

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            //ActivityCompat.requestPermissions(getActivity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location location2 = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

            if (location != null) {
                try{
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_UPDATE_TIME, MIN_UPDATE_DISTANCE, this);
                    latti = location.getLatitude();
                    longi = location.getLongitude();
                }catch (Exception e){
                    e.printStackTrace();
                    //Toast.makeText(context, R.string.un_network, Toast.LENGTH_SHORT).show();
                }
            } else if (location1 != null) {
                try{
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_UPDATE_TIME, MIN_UPDATE_DISTANCE, this);
                    latti = location1.getLatitude();
                    longi = location1.getLongitude();
                }catch (Exception e){
                    e.printStackTrace();
                    //Toast.makeText(context, R.string.un_gps, Toast.LENGTH_SHORT).show();
                }
            } else if (location2 != null) {
                try{
                    locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, MIN_UPDATE_TIME, MIN_UPDATE_DISTANCE,this);
                    latti = location2.getLatitude();
                    longi = location2.getLongitude();
                }catch (Exception e){
                    e.printStackTrace();
                    //Toast.makeText(context, R.string.un_passive_loc, Toast.LENGTH_SHORT).show();
                }
            } else {
                latti = 23.810571;
                longi = 90.412553;
                Toast.makeText(context, R.string.un_trace_loc, Toast.LENGTH_SHORT).show();
            }
            //Send_My_Current_Location_Once
            getMyLocation.getLocationOnce(String.valueOf(latti),String.valueOf(longi));
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        getMyLocation.getLocationOnce(String.valueOf(location.getLatitude()),String.valueOf(location.getLongitude()));
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) {
        Toast.makeText(context,R.string.gps_enable_status,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onProviderDisabled(String s) {
        Toast.makeText(context,R.string.gps_disable_status,Toast.LENGTH_LONG).show();
    }
}
