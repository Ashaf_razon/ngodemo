// Generated code from Butter Knife. Do not modify!
package maps_section;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.avis.avistechbd.ngodemo.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MyMapsActivity_ViewBinding implements Unbinder {
  private MyMapsActivity target;

  private View view2131230982;

  @UiThread
  public MyMapsActivity_ViewBinding(MyMapsActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MyMapsActivity_ViewBinding(final MyMapsActivity target, View source) {
    this.target = target;

    View view;
    target.searchServ = Utils.findRequiredViewAsType(source, R.id.mySearchServ, "field 'searchServ'", Spinner.class);
    target.searchMapFree = Utils.findRequiredViewAsType(source, R.id.mySearchMapFree, "field 'searchMapFree'", AppCompatImageView.class);
    view = Utils.findRequiredView(source, R.id.upArraw, "field 'upArraw' and method 'onUpArrawClicked'");
    target.upArraw = Utils.castView(view, R.id.upArraw, "field 'upArraw'", ImageView.class);
    view2131230982 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onUpArrawClicked();
      }
    });
    target.progressBar = Utils.findRequiredViewAsType(source, R.id.progressBar, "field 'progressBar'", ProgressBar.class);
    target.uDetection = Utils.findRequiredViewAsType(source, R.id.u_detection, "field 'uDetection'", ImageView.class);
    target.chargeView = Utils.findRequiredViewAsType(source, R.id.chargeSearv, "field 'chargeView'", TextView.class);
    target.ratingView = Utils.findRequiredViewAsType(source, R.id.ratingServ, "field 'ratingView'", TextView.class);
    target.addrsView = Utils.findRequiredViewAsType(source, R.id.addrsSearv, "field 'addrsView'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MyMapsActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.searchServ = null;
    target.searchMapFree = null;
    target.upArraw = null;
    target.progressBar = null;
    target.uDetection = null;
    target.chargeView = null;
    target.ratingView = null;
    target.addrsView = null;

    view2131230982.setOnClickListener(null);
    view2131230982 = null;
  }
}
